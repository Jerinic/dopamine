import React, { Component } from 'react'
import axios from 'axios'
import { CSSTransitionGroup } from 'react-transition-group'

import './Notifications.css'

export default class Notifications extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notifications: [],
      count: 0,
      isOpened: false
    }
  }
  componentDidMount () {
    axios.get('./fakeData.json')
    .then((res) => {
      const notifications = res.data
      const expires = 0
      const notificationsList = notifications.filter((item) => item.expires != expires)
      const notificationCount = notificationsList.filter((item) => item.type !== "bonus").length
      this.setState({notifications: notificationsList, count: notificationCount})
    })
  }

  removeElement = (item) => {
    const notifications = [...this.state.notifications]
    const newNotificationsList = notifications.filter((element) => {
      return element.id !== item.id
    })
    const notificationCount = newNotificationsList.filter((item) => {
      return item.type !== "bonus"
    }).length
    this.setState({notifications: newNotificationsList, count: notificationCount})
  }

  renderNotificationsList = () => {
    const notifications = [...this.state.notifications]
    if(notifications.length) {
      return notifications.map((item) => {
        return <li 
          className="notifications-list-item" 
          key={item.id}
          onClick={() => this.removeElement(item)}>
            <img className="notification-image" src={item.image} alt=""/>
            <p className="notification-title">{item.title}</p>
            <button className="delete" aria-label="close"></button>
          </li>
      })
    } else {
      return <li>no notifications</li>
    }
  }

  toggleNotificationList = () => {
    this.setState({isOpened: !this.state.isOpened})
  }

  render() {
    return (
      <div>
        {this.state.notifications ?
        <div className="Notifications">
          <button className="button is-primary" onClick={this.toggleNotificationList} > {!this.state.isOpened ? 'show' : 'hide'} notifications ({this.state.count})</button>
          {this.state.isOpened && 
          <ul className="notifications-list">
            <CSSTransitionGroup
            transitionName="notification"
            transitionAppear={true}
            transitionAppearTimeout={300}
            transitionEnterTimeout={300}
            transitionLeaveTimeout={300}>
              {this.renderNotificationsList()}
            </CSSTransitionGroup>
          </ul>}
        </div> 
        : null}
      </div>
    )
  }
}
