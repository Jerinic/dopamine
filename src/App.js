import React, { Component } from 'react';
import Notifications from './components/Notifications/Notifications'
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="container">
          <div className="columns is-multiline">
            <div className="column is-6-desktop">
              <Notifications />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
